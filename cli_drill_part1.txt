Drills Part 1: //Aditya Pandey

// Make a directory :- Hello

$ mkdir hello

//Change Directory(cd)

$ cd hello
//Create directory inside hello

$ mkdir five
$ mkdir one

//change Directory


$ cd five
//Create directory inside five

$ mkdir six

//Change directory

$ cd six

//create text file inside six

$ touch c.txt

$ mkdir seven

//change directory 

$ cd seven

// create log file inside seven

$ touch error.log

//change directory

cd ..

cd ..

cd ..

//Change directory

$ cd one

// create txt file and one directory

$ touch a.txt

$ touch b.txt

$ mkdir two

//change directory

$ cd two

// create txt file and directory

$ touch d.txt

$ mkdir three

//Change directory

$ cd three

//create txt and directory

$ touch e.txt

$ mkdir four

//Change directory

$ cd four

$ touch access.log

// change directory

cd ..
cd ..
cd ..

//Add the content to a.txt

$ vi a.txt

//Press i to insert and when finisher press ESC and then :wq to directly save your work and quit.

//delete log file

$ find -name "*.log" -type f -delete


//Change the directory
cd ..
// delete the directory named five

rm -R five